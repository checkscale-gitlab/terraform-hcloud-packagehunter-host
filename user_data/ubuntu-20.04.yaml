#cloud-config
package_update: true
package_upgrade: true
package_reboot_if_required: false

# Disable password authentication fo root user
ssh_pwauth: false

# Install required packages
packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - git
  - tmux
  - jq

groups:
  - docker
  - ecc-admin

users:
  - name: niclas
    gecos: 'EveryoneCanContribute Crew-Member'
    primary_group: ecc-admin
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin, docker
    lock_passwd: false
    ssh_authorized_keys:
     -  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOIvRdcEUBH4wOVJC+YB+q6cTYTgiUWXWx2y/likaXoo"

write_files:
- encoding: b64
  content: ${package_hunter_systemd_file_content}
  owner: root:root
  path: /etc/systemd/system/package-hunter.service
  permissions: '0755'

# Install Docker + Docker Compose & Start Docker
runcmd:
  - mkfs.${volume_filesystem} ${filesystem_cmd_opt} ${linux_device}
  - mkdir /mnt/${mount_dir_name}
  - mount -o discard,defaults ${linux_device} /mnt/${mount_dir_name}
  - echo '${linux_device} /mnt/${mount_dir_name} ${volume_filesystem} discard,nofail,defaults 0 0' >> /etc/fstab
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
  - curl -L "https://github.com/docker/compose/releases/download/${docker_compose_version}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  #- curl -fsSL https://raw.githubusercontent.com/MatchbookLab/local-persist/master/scripts/install.sh | bash
  - chmod +x /usr/local/bin/docker-compose
  - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  - apt-get update -y
  - apt-get install -y docker-ce docker-ce-cli containerd.io
  - sed -i -e "s|ExecStart=/usr/bin/dockerd|ExecStart=/usr/bin/dockerd --data-root=/mnt/${mount_dir_name}|g" /lib/systemd/system/docker.service
  - systemctl daemon-reload
  - systemctl restart docker
  - systemctl enable docker
  - curl -s https://falco.org/repo/falcosecurity-3672BA8F.asc | apt-key add -
  - echo "deb https://download.falco.org/packages/deb stable main" | tee -a /etc/apt/sources.list.d/falcosecurity.list
  - apt-get update -y
  - apt-get -y install linux-headers-$(uname -r)
  - apt-get install -y falco=0.29.1
  # Install Node v12.
  - wget https://nodejs.org/download/release/v12.22.6/node-v12.22.6-linux-x64.tar.gz
  - tar -C /usr/local --strip-components 1 -xzf node-v12.22.6-linux-x64.tar.gz
  - rm node-v12.22.6-linux-x64.tar.gz
  # Generate certificates for Falco.
  ## Create a RANDFILE for the root user.
  - openssl rand -writerand /root/.rnd
  ## Generate valid CA.
  - openssl genrsa -passout pass:1234 -des3 -out ca.key 4096
  - openssl req -passin pass:1234 -new -x509 -days 365 -key ca.key -out ca.crt -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Test/CN=Root CA"
  ## Generate valid Server Key/Cert.
  - openssl genrsa -passout pass:1234 -des3 -out server.key 4096
  - openssl req -passin pass:1234 -new -key server.key -out server.csr -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Server/CN=localhost"
  - openssl x509 -req -passin pass:1234 -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
  ## Remove passphrase from the Server Key.
  - openssl rsa -passin pass:1234 -in server.key -out server.key
  ## Generate valid Client Key/Cert.
  - openssl genrsa -passout pass:1234 -des3 -out client.key 4096
  - openssl req -passin pass:1234 -new -key client.key -out client.csr -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Client/CN=localhost"
  - openssl x509 -passin pass:1234 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt
  ## Remove passphrase from Client Key.
  - openssl rsa -passin pass:1234 -in client.key -out client.key
  ## Move files to correct location.
  - mkdir /etc/falco/certs
  - mv server.key /etc/falco/certs/
  - mv server.crt /etc/falco/certs/
  - mv ca.crt /etc/falco/certs/
  - mv client.key /etc/falco/certs/
  - mv client.crt /etc/falco/certs/
  - mv client.csr /etc/falco/certs/
  ## Make files world-readable.
  - chmod +r /etc/falco/certs/*
  - cd /etc/falco/
  - curl -O https://gitlab.com/gitlab-org/security-products/package-hunter/-/raw/main/falco/falco.yaml
  - curl -O https://gitlab.com/gitlab-org/security-products/package-hunter/-/raw/main/falco/falco_rules.yaml
  - curl -O https://gitlab.com/gitlab-org/security-products/package-hunter/-/raw/main/falco/falco_rules.local.yaml
  - cd /root
  # Install Falco driver
  - falco-driver-loader
  # Start Falco
  - service falco start
  # Load Falco on every boot.
  ## Load the kernel module/driver automatically on boot.
  - echo "falco" >> /etc/modules
  - depmod
  ## Start the Falco service automatically on boot
  - systemctl enable --now falco
  # Use latest Falco Version patch since GRPC has been updated
  - git clone --depth 1 https://gitlab.com/gitlab-org/security-products/package-hunter.git
  - cd package-hunter
  - npm ci
  - cp /etc/falco/certs/server.crt /etc/falco/certs/client.crt /etc/falco/certs/client.key .
  - cd /root
  # Build mal-dep image for executing all dependencies
  - cd package-hunter/falco-test
  - docker image build -t maldep .
  - cd /root
  - systemctl enable --now package-hunter

final_message: "The system is finally up, after $UPTIME seconds"
